## Objective ##

You've forked a repository, but you need to keep it up to date with the repository you forked from

## Exercise ##
Modify the examples here to keep your own fork of the tutorial repository up to date. Of course, if you're reading this, you may have already done that!

When you clone a repository you get a **remote** repository for free, which is where you push to with **git push**. You can add multiple remotes, and pull from/push to any of them, with only a minor modification to the syntax.

E.g. I have a fork of the **https://bitbucket.org/berkeleylab/jgi-lapinpy** repository, as **https://bitbucket.org/TWildish/jgi-lapinpy**. First I clone it:
```
> git clone git@bitbucket.org:TWildish/jgi-lapinpy.git
Cloning into 'jgi-lapinpy'...
remote: Counting objects: 1320, done.
remote: Compressing objects: 100% (1305/1305), done.
remote: Total 1320 (delta 800), reused 0 (delta 0)
Receiving objects: 100% (1320/1320), 2.32 MiB | 603.00 KiB/s, done.
Resolving deltas: 100% (800/800), done.
```

Next, **cd** into the clone, and check the remotes that are available there:
```
> cd jgi-lapinpy/
> git remote -v show
origin  git@bitbucket.org:TWildish/jgi-lapinpy.git (fetch)
origin  git@bitbucket.org:TWildish/jgi-lapinpy.git (push)
```

You can see that I can **fetch** (pull) from the **origin** remote, and I can **push** to it too. **origin** is the default remote name, so when I **git pull** or **git push**, it defaults to using this remote.

Let's add the original repository as a new remote, with the arbitrarily-chosen name **upstream**:

```
> git remote add upstream git@bitbucket.org:berkeleylab/jgi-lapinpy.git

> git remote -v show
origin  git@bitbucket.org:TWildish/jgi-lapinpy.git (fetch)
origin  git@bitbucket.org:TWildish/jgi-lapinpy.git (push)
upstream  git@bitbucket.org:berkeleylab/jgi-lapinpy.git (fetch)
upstream  git@bitbucket.org:berkeleylab/jgi-lapinpy.git (push)
```

Now I can pull from the *upstream master** branch, and keep my own branch up-to-date with the original source:

```
> git pull upstream master
From bitbucket.org:berkeleylab/jgi-lapinpy
 * branch            master     -> FETCH_HEAD
Updating a3f5e1e..03943c8
Fast-forward
```

For more info, look at section 2.5 of "Pro Git", by Scott Chacon. You can find that free on the web.