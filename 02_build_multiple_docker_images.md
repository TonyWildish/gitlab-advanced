## Objective ##

Build more than one docker image in a single CI build

## Exercise ##

Copy **02-gitlab-ci.yml** to **.gitlab-ci.yml**, update the **REGISTRY_USER** and commit, push.

Check the **Pipelines** page, you will see a single pipeline being processed.

Check the **Registry** page, you'll see pulldown menus for each image name

To pull/run these images, specify the full name as you did in the **.gitlab-ci.yml** file:
```
docker pull registry.gitlab.com/<your-username>/gitlab-advanced/myapp-full:latest
```

You can tag and branch, as before, and those tags/branch labels are propagated to each image.

## Best practices ##

- build multiple images when you have distinct use-cases, e.g. when 90% of your users will need only a small subset of functionality, but the other users will need the whole package.
- consider the tradeoff in size vs. confusion. Is it worth making the user think which version they want if it only saves a few MB? On the other hand, if it saves several hundred MB, it might well be worth it